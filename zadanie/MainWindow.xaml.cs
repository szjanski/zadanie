﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;

namespace zadanie
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public ObservableCollection<Operation> operations { get; set; }

        public MainWindow()
        {
            InitializeComponent();
            operations = new ObservableCollection<Operation>();
            operationsData.ItemsSource = operations;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ErrorLbl.Content = "";
                if (!ValidateInputs())
                {
                    ErrorLbl.Content = "Podaj poprawne liczby całkowite";
                    return;
                }
                var a = Int32.Parse(FirstNumber.Text);
                var b = Int32.Parse(SecondNumber.Text);
                int result = 0;
                switch (Operation.Text)
                {
                    case "+":
                        result = WebServiceConnector.Add(a, b);
                        break;
                    case "-":
                        result = WebServiceConnector.Substract(a, b);
                        break;
                    case "*":
                        result = WebServiceConnector.Multiply(a, b);
                        break;
                }
                operations.Add(new Operation
                {
                    FirstNumber = a,
                    SecondNumber = b,
                    Type = Operation.Text,
                    Result = result
                });
            }
            catch(Exception ex)
            {
                ErrorLbl.Content = "Błąd";
                Console.Error.WriteLine($"{ex.Message} {ex.StackTrace}");
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            try
            {
                var fs = new FileStream(@"operations.xml", FileMode.OpenOrCreate);
                var serializer = new System.Xml.Serialization.XmlSerializer(typeof(ObservableCollection<Operation>));
                serializer.Serialize(fs, operations);
                MessageBox.Show("Serializacja zakończona", "Info", MessageBoxButton.OK);
            }
            catch (Exception ex)
            {
                ErrorLbl.Content = "Błąd";
                Console.Error.WriteLine($"{ex.Message} {ex.StackTrace}");
            }
        }

        private bool ValidateInputs()
        {
            return Regex.Match(FirstNumber.Text, @"^[-]?\d*$").Length > 0 && Regex.Match(SecondNumber.Text, @"^[-]?\d*$").Length > 0;
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            try
            {
                var xmlData = SqlConnector.GetXmlData();
                if(String.IsNullOrEmpty(xmlData))
                {
                    MessageBox.Show("brak danych w bazie", "Info", MessageBoxButton.OK);
                }
                else
                {
                    var serializer = new System.Xml.Serialization.XmlSerializer(typeof(ObservableCollection<Operation>));
                    var data = (ObservableCollection<Operation>)serializer.Deserialize(new MemoryStream(Encoding.UTF8.GetBytes(xmlData)));
                    foreach (var item in data)
                        operations.Add(item);
                }
            }
            catch(Exception ex)
            {
                ErrorLbl.Content = "Błąd";
                Console.Error.WriteLine($"{ex.Message} {ex.StackTrace}");
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadanie
{
    public static class SqlConnector
    {
        public static string GetXmlData()
        {
            string data = String.Empty;
            using (SqlConnection connection =
               new SqlConnection("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=zadanie;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False"))
            {
                connection.Open();

                using (SqlCommand command =
                    new SqlCommand("SELECT * FROM OperationsXml WHERE CreatedDate IN(SELECT max(CreatedDate) FROM OperationsXml)", connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            data = (reader["XmlData"].ToString());
                        }

                        reader.Close();
                    }
                }
                return data;
            }
        }
    }
}

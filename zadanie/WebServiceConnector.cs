﻿using System;
using System.IO;
using System.Net;
using System.Xml;

namespace zadanie
{
    public static class WebServiceConnector
    {
        private static string requestBody = @"<?xml version=""1.0"" encoding=""utf-8""?>
<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">
  <soap:Body>
    <{2} xmlns=""http://tempuri.org/"">
      <intA>{0}</intA>
      <intB>{1}</intB>
    </{2}>
  </soap:Body>
</soap:Envelope>";

        public static int Add(int a, int b)
        {
            var operationName = "Add";
            var body = requestBody.Replace("{0}", a.ToString()).Replace("{1}", b.ToString()).Replace("{2}", operationName);
            var request = CreateWebRequest(operationName);
            return ParseResult(Execute(body, request), operationName);
        }

        public static int Substract(int a, int b)
        {
            var operationName = "Subtract";
            var body = requestBody.Replace("{0}", a.ToString()).Replace("{1}", b.ToString()).Replace("{2}", operationName);
            var request = CreateWebRequest(operationName);
            return ParseResult(Execute(body, request), operationName);
        }

        public static int Multiply(int a, int b)
        {
            var operationName = "Multiply";
            var body = requestBody.Replace("{0}", a.ToString()).Replace("{1}", b.ToString()).Replace("{2}", operationName);
            var request = CreateWebRequest(operationName);
            return ParseResult(Execute(body, request), operationName);
        }

        private static int ParseResult(string xmlBody, string operationName)
        {
            var xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xmlBody);
            return Int32.Parse(xmlDoc.GetElementsByTagName($"{operationName}Result")[0].InnerText);
        }

        private static string Execute(string body, HttpWebRequest request)
        {
            var xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(body);

            using (Stream stream = request.GetRequestStream())
            {
                xmlDoc.Save(stream);
            }

            using (WebResponse response = request.GetResponse())
            {
                using (StreamReader rd = new StreamReader(response.GetResponseStream()))
                {
                    return rd.ReadToEnd();
                }
            }
        }

        private static HttpWebRequest CreateWebRequest(string operationName)
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create($@"http://www.dneonline.com/calculator.asmx?op={operationName}");
            webRequest.Headers.Add($@"SOAPAction:""http://tempuri.org/{operationName}""");
            webRequest.ContentType = "text/xml;charset=\"utf-8\"";
            webRequest.Accept = "text/xml";
            webRequest.Method = "POST";
            return webRequest;
        }

    }
}
